// **Iteración #6: Función swap**

// Crea una función llamada `swap()` que reciba un array y dos parametros que sean indices del array. La función deberá intercambiar la posición de los valores de los indices que hayamos enviado como parametro. Retorna el array resultante.

// Sugerencia de array:

let list=['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño'];

function swap(param,a,b){
    let i=param[a];
    param.splice(a,1,param[b]);
    param.splice(b,1,i);
    return param;
}
console.log(swap(list,0,3));
